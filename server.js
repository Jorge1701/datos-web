const express = require('express');
const app = express();

// Run the app by serving the static files in the dist directory
app.use(express.static(__dirname + '/dist/datos-web'));
app.use((req, res) => {
  res.sendFile(__dirname + '/dist/datos-web/index.html');
});

// Start the app by listening on the default Heroku port
app.listen(process.env.PORT || 4200);
