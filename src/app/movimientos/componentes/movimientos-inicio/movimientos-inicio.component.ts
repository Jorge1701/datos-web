import { Component, OnInit } from '@angular/core';
import { MovimientosModel } from '../../modelo/movimientos.model';
import { MovimientosServicio } from '../../servicios/movimientos-servicio';
import { DialogoServicio } from '../../../core/servicios/dialogo-servicio';
import { SnackServicio } from '../../../core/servicios/snack-servicio';
import { FormularioDinamicoServicio } from '../../../core/servicios/formulario-dinamico-servicio';
import { CampoFormulario } from '../../../core/componentes/formulario-dinamico/formulario-dinamico.component';
import { ActivatedRoute } from '@angular/router';
import { CuentaModel } from '../../../cuentas/modelo';
import { CategoriaModel } from '../../../categorias/modelo';

@Component({
  selector: 'app-movimientos-inicio',
  templateUrl: './movimientos-inicio.component.html',
  styleUrls: ['./movimientos-inicio.component.css']
})
export class MovimientosInicioComponent implements OnInit {
  cuentas: CuentaModel[] | undefined;
  categorias: CategoriaModel[] | undefined;
  movimientos: MovimientosModel[] | undefined;

  tiposMovimientos = [
    { nombre: 'Ingreso', clave: 'ingreso' },
    { nombre: 'Gasto', clave: 'gasto' }
  ];

  constructor(
    private movimientosServicio: MovimientosServicio,
    private dialogo: DialogoServicio,
    private snackServicio: SnackServicio,
    private formularioDinamicoServicio: FormularioDinamicoServicio,
    private activatedRoute: ActivatedRoute,
    private snack: SnackServicio
  ) { }

  ngOnInit(): void {
    this.cuentas = this.activatedRoute.snapshot.data.cuentas;
    this.categorias = this.activatedRoute.snapshot.data.categorias;
    // TODO al hacer refactor de tabla, la misma se encargara de esto
    this.movimientosServicio.getList().subscribe((movimientos: MovimientosModel[]) => {
      this.movimientos = movimientos;
    });
  }

  borrar(movimiento: MovimientosModel): void {
    this.dialogo.pregunta(`¿Borrar movimiento?`)
      .subscribe((confirmado: boolean) => {
        if (confirmado && movimiento._id) {
          this.borrarMovimiento(movimiento._id);
        }
      });
  }

  private borrarMovimiento(id: string): void {
    this.movimientosServicio.delete(id)
      .subscribe((movimientoBorrado: MovimientosModel) => {
        if (this.movimientos) {
          this.movimientos = this.movimientos.filter((movimiento: MovimientosModel) => {
            return movimiento._id !== movimientoBorrado._id;
          });
        }
        this.snackServicio.exito('Movimiento borrado');
      });
  }

  abrir(): void {
    const campos: CampoFormulario[] = [
      { nombre: 'Fecha', clave: 'fecha', tipo: 'date', valor: new Date() },
      { nombre: 'Cuenta', clave: 'cuenta', tipo: 'select', elementros: this.cuentas, claveElemento: 'nombre' },
      { nombre: 'Tipo', clave: 'tipo', tipo: 'select', elementros: this.tiposMovimientos, claveElemento: 'nombre', valorElemento: 'clave' },
      { nombre: 'Cantidad', clave: 'cantidad', tipo: 'number' },
      { nombre: 'Categoría', clave: 'categoria', tipo: 'select', elementros: this.categorias, claveElemento: 'nombre' },
      { nombre: 'Nota', clave: 'nota', requerido: false },
    ];
    this.formularioDinamicoServicio.obtener<MovimientosModel>('Registrar movimiento', campos, 'Registrar')
      .subscribe((movimiento: MovimientosModel) => {
        if (movimiento) {
          this.movimientosServicio.registrar(movimiento).subscribe((movimientoAgregado: MovimientosModel) => {
            if (this.movimientos) {
              // TODO Actualizar lista luego de refactor
              this.movimientos = [movimiento, ...this.movimientos];
            }
            this.snack.exito(`Agregado ${movimientoAgregado.tipo} por $ ${movimientoAgregado.cantidad}`);
          }, error => {
            this.snack.error(error.error);
          });
        }
      });
  }
}
