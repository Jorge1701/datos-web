import { CuentaModel } from '../../cuentas/modelo';
import { CategoriaModel } from '../../categorias/modelo';

export class MovimientosModel {
  constructor(
    public cuenta: CuentaModel,
    public tipo: string,
    public cantidad: number,
    public categoria: CategoriaModel,
    public nota: string,
    public fecha: Date,
    public _id?: string
  ) {}
}
