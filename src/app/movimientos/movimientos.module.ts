import { NgModule } from '@angular/core';
import { CoreModule } from '../core/core.module';
import { MovimientosInicioComponent } from './componentes/movimientos-inicio/movimientos-inicio.component';
import { MovimientosRoutingModule } from './movimientos-routing.module';
import { MovimientosServicio } from './servicios/movimientos-servicio';
import { CuentasModule } from '../cuentas/cuentas.module';
import { CategoriasModule } from '../categorias/categorias.module';

@NgModule({
  imports: [
    CoreModule,
    MovimientosRoutingModule,
    CuentasModule,
    CategoriasModule
  ],
  declarations: [
    MovimientosInicioComponent
  ],
  providers: [
    MovimientosServicio
  ]
})
export class MovimientosModule { }
