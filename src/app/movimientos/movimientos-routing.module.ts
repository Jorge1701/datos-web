import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MovimientosInicioComponent } from './componentes/movimientos-inicio/movimientos-inicio.component';
import { CuentasResolver } from '../cuentas/servicios/cuentas-resolver';
import { CategoriasResolver } from '../categorias/servicios/categorias-resolver';

const routes: Routes = [
  {
    path: '', component: MovimientosInicioComponent,
    resolve: {
      cuentas: CuentasResolver,
      categorias: CategoriasResolver
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MovimientosRoutingModule { }
