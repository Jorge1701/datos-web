import { Injectable } from '@angular/core';
import { ApiServicio } from '../../core/servicios/api-servicio';
import { HttpClient } from '@angular/common/http';
import { MovimientosModel } from '../modelo/movimientos.model';
import { Observable } from 'rxjs';

@Injectable()
export class MovimientosServicio extends ApiServicio<MovimientosModel> {
  constructor(
    protected http: HttpClient
  ) {
    super(http, 'movimientos');
  }

  public registrar(movimiento: MovimientosModel): Observable<MovimientosModel> {
    return this.post(movimiento) as Observable<MovimientosModel>;
  }
}
