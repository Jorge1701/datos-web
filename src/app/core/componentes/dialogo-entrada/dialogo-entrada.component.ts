import { Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';

export interface DialogoEntrada {
  tipo: 'entrada' | 'pregunta';
  valorAnterior?: string;
  nombreEntrada?: string;
  descripcion?: string;
  pregunta?: string;
}

@Component({
  selector: 'app-dialogo-entrada',
  templateUrl: './dialogo-entrada.component.html',
  styleUrls: ['./dialogo-entrada.component.css']
})
export class DialogoEntradaComponent {
  valor: string | undefined;

  constructor(
    public matDialogRef: MatDialogRef<DialogoEntradaComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogoEntrada
  ) {
    if (data && data.valorAnterior) {
      this.valor = data.valorAnterior;
    }
  }

  cerrar(): void {
    this.matDialogRef.close();
  }
}
