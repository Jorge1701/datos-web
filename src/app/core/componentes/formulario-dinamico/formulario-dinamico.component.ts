import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { AbstractControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MomentDateAdapter } from '@angular/material-moment-adapter';
import { DateAdapter, MAT_DATE_LOCALE, MAT_DATE_FORMATS } from '@angular/material/core';
import { SnackServicio } from '../../servicios/snack-servicio';

export interface DatosFormulario {
  titulo: string;
  textoAceptar: string;
  textoCancelar: string;
  campos: CampoFormulario[];
}

export interface CampoFormulario {
  nombre: string;
  clave: string;
  requerido?: boolean;
  tipo?: string;
  elementros?: any[];
  valorElemento?: string;
  claveElemento?: string;
  valor?: any;
}

interface Resultado {
  [key: string]: any;
}

export const FORMATOS_FECHA = {
  parse: {
    dateInput: 'LL'
  },
  display: {
    dateInput: 'DD-MM-YYYY',
    monthYearLabel: 'YYYY',
    dateA11yLabel: 'LL',
    monthYearA11yLabel: 'YYYY'
  }
};

@Component({
  selector: 'app-formulario-dinamico',
  templateUrl: './formulario-dinamico.component.html',
  styleUrls: ['./formulario-dinamico.component.css'],
  providers: [
    { provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE] },
    { provide: MAT_DATE_FORMATS, useValue: FORMATOS_FECHA }
  ]
})
export class FormularioDinamicoComponent implements OnInit {
  formulario: FormGroup = this.fb.group({});

  private static getValor(valor: any, tipo: string): any {
    if (tipo === 'number') {
      return +valor;
    } else if (tipo === 'date') {
      return new Date(valor);
    } else {
      return valor;
    }
  }

  constructor(
    private fb: FormBuilder,
    private snack: SnackServicio,
    private matDialogRef: MatDialogRef<FormularioDinamicoComponent>,
    @Inject(MAT_DIALOG_DATA) public datos: DatosFormulario
  ) {
  }

  ngOnInit(): void {
    this.datos.campos.forEach((campo: CampoFormulario) => {
      const campoRequerido: boolean = campo.requerido === undefined || campo.requerido;
      this.formulario.addControl(campo.clave, this.fb.control(campo.valor, campoRequerido ? [Validators.required] : []));
    });
  }

  terminar(): void {
    this.formulario.markAllAsTouched();
    if (this.formulario.invalid) {
      return this.snack.error('Faltan campos');
    }
    const resultado: Resultado = {};
    this.datos.campos.forEach((campo: CampoFormulario) => {
      const control: AbstractControl = this.formulario.controls[campo.clave];
      if (campo.tipo) {
        resultado[campo.clave] = FormularioDinamicoComponent.getValor(control.value, campo.tipo);
      } else {
        resultado[campo.clave] = control.value;
      }
    });
    this.matDialogRef.close(resultado);
  }

  cerrar(): void {
    this.matDialogRef.close();
  }
}
