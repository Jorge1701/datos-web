import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../../environments/environment';

export abstract class ApiServicio<T> {
  protected baseUrl: string;

  protected constructor(
    protected http: HttpClient,
    protected path: string
  ) {
    this.baseUrl = environment.apiUrl + path;
    if (!this.baseUrl.endsWith('/')) {
      this.baseUrl = this.baseUrl + '/';
    }
  }

  public get(url?: string, params?: any): Observable<T> {
    return this.http.get<T>(this.getUrl(url), { params });
  }

  public getList(url?: string, params?: any): Observable<T[]> {
    return this.http.get<T[]>(this.getUrl(url), { params });
  }

  public post(request: any, url?: string): Observable<T | void> {
    return this.http.post<T | void>(this.getUrl(url), request);
  }

  public patch(request: any, url?: string): Observable<T | void> {
    return this.http.patch<T | void>(this.getUrl(url), request);
  }

  public delete(id: any, url?: string): Observable<T> {
    return this.http.delete<T>(this.getUrl(url), { params: { id } });
  }

  protected getUrl(url?: string): string {
    return !url ? this.baseUrl : this.baseUrl + url;
  }
}
