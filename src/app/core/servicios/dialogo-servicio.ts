import { MatDialog } from '@angular/material/dialog';
import { DialogoEntrada, DialogoEntradaComponent } from '../componentes/dialogo-entrada/dialogo-entrada.component';
import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';

@Injectable()
export class DialogoServicio {
  constructor(
    private dialog: MatDialog
  ) {}

  public entrada(valorAnterior: string, nombreEntrada: string, descripcion: string): Observable<string> {
    const data: DialogoEntrada = {
      tipo: 'entrada',
      valorAnterior,
      nombreEntrada,
      descripcion
    };
    return this.dialog.open(DialogoEntradaComponent, { data }).afterClosed();
  }

  public pregunta(pregunta: string): Observable<boolean> {
    const data: DialogoEntrada = {
      tipo: 'pregunta',
      pregunta
    };
    return this.dialog.open(DialogoEntradaComponent, { data }).afterClosed();
  }
}
