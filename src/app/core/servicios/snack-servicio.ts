import { MatSnackBar } from '@angular/material/snack-bar';
import { Injectable } from '@angular/core';

@Injectable()
export class SnackServicio {
  accionPorDefecto = 'OK';
  duracionPorDefecto = 3500;

  constructor(
    private snackBar: MatSnackBar
  ) {}

  public info(mensaje: string, accion?: string): void {
    this.snackBar.open(mensaje, accion ? accion : this.accionPorDefecto, {
      duration: this.duracionPorDefecto
    });
  }

  public exito(mensaje: string, accion?: string): void {
    this.snackBar.open(mensaje, accion ? accion : this.accionPorDefecto, {
      duration: this.duracionPorDefecto,
      panelClass: ['exito-snack']
    });
  }

  public error(mensaje: string, accion?: string): void {
    this.snackBar.open(mensaje, accion ? accion : this.accionPorDefecto, {
      duration: this.duracionPorDefecto,
      panelClass: ['error-snack']
    });
  }
}
