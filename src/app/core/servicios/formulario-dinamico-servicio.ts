import { Injectable } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Observable } from 'rxjs';
import { FormularioDinamicoComponent, CampoFormulario } from '../componentes/formulario-dinamico/formulario-dinamico.component';

@Injectable()
export class FormularioDinamicoServicio {
  constructor(
    private dialog: MatDialog
  ) {}

  public obtener<T>(titulo: string, campos: CampoFormulario[], objeto?: any, textoAceptar?: string, textoCancelar?: string): Observable<T> {
    if (objeto) {
      campos = this.rellenarCampos(campos, objeto);
    }
    return this.dialog.open(FormularioDinamicoComponent, {
      data: {
        titulo, campos,
        textoAceptar: textoAceptar ? textoAceptar : 'Aceptar',
        textoCancelar: textoCancelar ? textoCancelar : 'Cancelar'
      },
      width: '100%'
    }).afterClosed();
  }

  private rellenarCampos(campos: CampoFormulario[], cuenta: any): CampoFormulario[] {
    return campos.map((campo: CampoFormulario) => {
      if (cuenta) {
        campo.valor = cuenta[campo.clave];
      }
      return campo;
    });
  }
}
