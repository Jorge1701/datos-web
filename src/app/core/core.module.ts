import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatCardModule } from '@angular/material/card';
import { MatButtonModule } from '@angular/material/button';
import { FlexLayoutModule } from '@angular/flex-layout';
import { CommonModule } from '@angular/common';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Overlay } from '@angular/cdk/overlay';
import { SnackServicio } from './servicios/snack-servicio';
import { MatTableModule} from '@angular/material/table';
import { MatIconModule } from '@angular/material/icon';
import { DialogoEntradaComponent } from './componentes/dialogo-entrada/dialogo-entrada.component';
import { MatDialogModule } from '@angular/material/dialog';
import { FormBuilder, FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { MatListModule } from '@angular/material/list';
import { DialogoServicio } from './servicios/dialogo-servicio';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatSidenavModule } from '@angular/material/sidenav';
import { CabeceraComponent } from './componentes/cabecera/cabecera.component';
import { MatSelectModule } from '@angular/material/select';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatNativeDateModule } from '@angular/material/core';
import { MatGridListModule } from '@angular/material/grid-list';
import { FormularioDinamicoComponent } from './componentes/formulario-dinamico/formulario-dinamico.component';
import { FormularioDinamicoServicio } from './servicios/formulario-dinamico-servicio';

@NgModule({
  imports: [
    HttpClientModule,
    MatFormFieldModule,
    MatInputModule,
    MatCardModule,
    MatButtonModule,
    FlexLayoutModule,
    CommonModule,
    MatTableModule,
    MatIconModule,
    MatDialogModule,
    MatFormFieldModule,
    FormsModule,
    MatProgressBarModule,
    MatListModule,
    MatToolbarModule,
    MatSidenavModule,
    MatSelectModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatGridListModule,
    ReactiveFormsModule
  ],
  exports: [
    MatFormFieldModule,
    MatInputModule,
    MatCardModule,
    MatButtonModule,
    FlexLayoutModule,
    CommonModule,
    MatTableModule,
    MatIconModule,
    MatDialogModule,
    MatFormFieldModule,
    FormsModule,
    MatProgressBarModule,
    MatListModule,
    MatToolbarModule,
    MatSidenavModule,
    CabeceraComponent,
    MatSelectModule,
    MatDatepickerModule,
    MatGridListModule
  ],
  providers: [
    MatSnackBar,
    Overlay,
    SnackServicio,
    DialogoServicio,
    FormularioDinamicoServicio,
    FormBuilder
  ],
  declarations: [DialogoEntradaComponent, CabeceraComponent, FormularioDinamicoComponent]
})
export class CoreModule { }
