import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CategoriasInicioComponent } from './componentes/categorias-inicio/categorias-inicio.component';

const routes: Routes = [
  { path: '', component: CategoriasInicioComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CategoriasRoutingModule { }
