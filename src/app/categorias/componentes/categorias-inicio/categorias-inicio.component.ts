import { Component, OnInit } from '@angular/core';
import { SnackServicio } from '../../../core/servicios/snack-servicio';
import { DialogoServicio } from '../../../core/servicios/dialogo-servicio';
import { CategoriaModel } from '../../modelo';
import { CategoriasServicio } from '../../servicios/categorias-servicio';

@Component({
  selector: 'app-categorias-inicio',
  templateUrl: './categorias-inicio.component.html',
  styleUrls: ['./categorias-inicio.component.css']
})
export class CategoriasInicioComponent implements OnInit {

  nombre: string | undefined;
  categorias: CategoriaModel[] | undefined;

  constructor(
    private snackServicio: SnackServicio,
    private dialogo: DialogoServicio,
    private categoriasServicio: CategoriasServicio
  ) { }

  ngOnInit(): void {
    this.categoriasServicio.getList().subscribe((categorias: CategoriaModel[]) => {
      this.categorias = categorias;
    });
  }

  agregar(): void {
    if (!!this.nombre) {
      this.categoriasServicio.agregar(this.nombre).subscribe((categoria: CategoriaModel) => {
        if (this.categorias) {
          this.categorias = [...this.categorias, categoria];
        }
        this.nombre = '';
        this.snackServicio.exito('Categoria agregada');
      }, error => {
        this.snackServicio.error(error.error);
      });
    } else {
      this.snackServicio.error('Ingrese un nombre');
    }
  }

  editar(categoria: CategoriaModel): void {
    this.dialogo.entrada(categoria.nombre, 'Nombre', 'Cambiar nombre de categoria')
      .subscribe((nuevoNombre: string) => {
        if (nuevoNombre && nuevoNombre.trim() !== '') {
          if (nuevoNombre === categoria.nombre) {
            this.snackServicio.exito('Nombre cambiado');
          } else {
            this.editarCategoria(categoria._id, nuevoNombre);
          }
        }
      });
  }

  private editarCategoria(id: string, nombre: string): void {
    this.categoriasServicio.cambiarNombre(id, nombre)
      .subscribe((categoria: CategoriaModel) => {
        if (this.categorias) {
          this.categorias = this.categorias.map(c => {
            if (c._id === categoria._id) {
              return categoria;
            } else {
              return c;
            }
          });
        }
        this.snackServicio.exito('Nombre cambiado');
      }, error => {
        this.snackServicio.error(error.error);
      });
  }

  borrar(categoria: CategoriaModel): void {
    this.dialogo.pregunta(`¿Borrar categoria ${categoria.nombre}?`)
      .subscribe((confirmado: boolean) => {
        if (confirmado) {
          this.borrarCategoria(categoria._id);
        }
      });
  }

  private borrarCategoria(id: string): void {
    this.categoriasServicio.delete(id)
      .subscribe((categoriaBorrada: CategoriaModel) => {
        if (this.categorias) {
          this.categorias = this.categorias.filter((categoria: CategoriaModel) => {
            return categoria._id !== categoriaBorrada._id;
          });
        }
        this.snackServicio.exito('Categoria borrada');
      });
  }
}
