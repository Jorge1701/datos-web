import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { CategoriaModel } from '../modelo';
import { Observable } from 'rxjs';
import { CategoriasServicio } from './categorias-servicio';

@Injectable()
export class CategoriasResolver implements Resolve<CategoriaModel[]> {
  constructor(
    private categoriasServicio: CategoriasServicio
  ) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot)
    : Observable<CategoriaModel[]> | Promise<CategoriaModel[]> | CategoriaModel[] {
    return this.categoriasServicio.getList();
  }
}
