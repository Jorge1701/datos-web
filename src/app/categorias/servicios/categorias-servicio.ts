import { ApiServicio } from '../../core/servicios/api-servicio';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { CategoriaModel } from '../modelo';
import { Observable } from 'rxjs';

@Injectable()
export class CategoriasServicio extends ApiServicio<CategoriaModel> {
  constructor(
    protected http: HttpClient
  ) {
    super(http, 'categorias');
  }

  public agregar(nombre: string): Observable<CategoriaModel> {
    return this.post({ nombre }) as Observable<CategoriaModel>;
  }

  public cambiarNombre(id: string, nombre: string): Observable<CategoriaModel> {
    return this.patch({ id, nombre }, 'nombre') as Observable<CategoriaModel>;
  }
}
