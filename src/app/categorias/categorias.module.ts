import { NgModule } from '@angular/core';
import { CoreModule } from '../core/core.module';
import { CategoriasRoutingModule } from './categorias-routing.module';
import { CategoriasInicioComponent } from './componentes/categorias-inicio/categorias-inicio.component';
import { CategoriasServicio } from './servicios/categorias-servicio';
import { CategoriasResolver } from './servicios/categorias-resolver';

@NgModule({
  imports: [
    CoreModule,
    CategoriasRoutingModule
  ],
  declarations: [
    CategoriasInicioComponent
  ],
  providers: [
    CategoriasServicio,
    CategoriasResolver
  ]
})
export class CategoriasModule { }
