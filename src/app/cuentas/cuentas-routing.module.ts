import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CuentasInicioComponent } from './componentes/cuentas-inicio/cuentas-inicio.component';

const routes: Routes = [
  { path: '', component: CuentasInicioComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CuentasRoutingModule { }
