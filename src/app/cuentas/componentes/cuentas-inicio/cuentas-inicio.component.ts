import { Component, OnInit } from '@angular/core';
import { CuentasServicio } from '../../servicios/cuentas-servicio';
import { CuentaModel } from '../../modelo';
import { SnackServicio } from '../../../core/servicios/snack-servicio';
import { DialogoServicio } from '../../../core/servicios/dialogo-servicio';
import { FormularioDinamicoServicio } from '../../../core/servicios/formulario-dinamico-servicio';
import { CampoFormulario } from '../../../core/componentes/formulario-dinamico/formulario-dinamico.component';

@Component({
  selector: 'app-datos-inicio',
  templateUrl: './cuentas-inicio.component.html',
  styleUrls: ['./cuentas-inicio.component.css']
})
export class CuentasInicioComponent implements OnInit {
  cuentas: CuentaModel[] | undefined;
  campos: CampoFormulario[] = [
    { nombre: 'Nombre', clave: 'nombre' },
    { nombre: 'Saldo', clave: 'saldo', tipo: 'number' }
  ];

  constructor(
    private snackServicio: SnackServicio,
    private dialogo: DialogoServicio,
    private datosServicio: CuentasServicio,
    private formularioDinamicoServicio: FormularioDinamicoServicio
  ) { }

  ngOnInit(): void {
    this.datosServicio.getList().subscribe((cuentas: CuentaModel[]) => {
      this.cuentas = cuentas;
    });
  }

  crearCuenta(): void {
    this.formularioDinamicoServicio.obtener<CuentaModel>('Crear cuenta', this.campos, 'Crear')
      .subscribe((cuenta: CuentaModel) => {
        if (cuenta) {
          this.datosServicio.agregar(cuenta).subscribe((cuentaNueva: CuentaModel) => {
            if (this.cuentas) {
              this.cuentas = [...this.cuentas, cuentaNueva];
            }
            this.snackServicio.exito('Cuenta agregada');
          }, error => {
            this.snackServicio.error(error.error);
          });
        }
      });
  }

  editar(cuentaEditar: CuentaModel): void {
    this.formularioDinamicoServicio.obtener<CuentaModel>('Editar cuenta', this.campos, cuentaEditar, 'Editar')
      .subscribe((cuentaEditada: CuentaModel) => {
        if (cuentaEditada && cuentaEditar._id) {
          cuentaEditada._id = cuentaEditar._id;
          this.datosServicio.editar(cuentaEditada)
            .subscribe((cuenta: CuentaModel) => {
              if (this.cuentas) {
                this.cuentas = this.cuentas.map(c => {
                  if (c._id === cuenta._id) {
                    return cuenta;
                  } else {
                    return c;
                  }
                });
              }
              this.snackServicio.exito('Cuenta editada');
            }, error => {
              this.snackServicio.error(error.error);
            });
        }
      });
  }

  borrar(cuenta: CuentaModel): void {
    this.dialogo.pregunta(`¿Borrar cuenta ${cuenta.nombre}?`)
      .subscribe((confirmado: boolean) => {
        if (confirmado && cuenta._id) {
          this.borrarCuenta(cuenta._id);
        }
      });
  }

  private borrarCuenta(id: string): void {
    this.datosServicio.delete(id)
      .subscribe((cuentaBorrada: CuentaModel) => {
        if (this.cuentas) {
          this.cuentas = this.cuentas.filter((cuenta: CuentaModel) => {
            return cuenta._id !== cuentaBorrada._id;
          });
        }
        this.snackServicio.exito('Cuenta borrada');
      });
  }
}
