export class CuentaModel {
  constructor(
    public nombre: string,
    public saldo: number,
    public _id?: string
  ) {
  }
}
