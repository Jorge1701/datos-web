import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { CuentaModel } from '../modelo';
import { ApiServicio } from '../../core/servicios/api-servicio';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class CuentasServicio extends ApiServicio<CuentaModel> {
  constructor(
    protected http: HttpClient
  ) {
    super(http, 'cuentas');
  }

  public agregar(cuenta: CuentaModel): Observable<CuentaModel> {
    return this.post(cuenta) as Observable<CuentaModel>;
  }

  public editar(cuenta: CuentaModel): Observable<CuentaModel> {
    return this.patch(cuenta) as Observable<CuentaModel>;
  }
}
