import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { CuentaModel } from '../modelo';
import { Observable } from 'rxjs';
import { CuentasServicio } from './cuentas-servicio';

@Injectable()
export class CuentasResolver implements Resolve<CuentaModel[]> {
  constructor(
    private cuentasServicio: CuentasServicio
  ) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot)
    : Observable<CuentaModel[]> | Promise<CuentaModel[]> | CuentaModel[] {
    return this.cuentasServicio.getList();
  }
}
