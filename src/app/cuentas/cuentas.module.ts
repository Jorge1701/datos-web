import { NgModule } from '@angular/core';
import { CuentasInicioComponent } from './componentes/cuentas-inicio/cuentas-inicio.component';
import { CuentasRoutingModule } from './cuentas-routing.module';
import { CuentasServicio } from './servicios/cuentas-servicio';
import { CoreModule } from '../core/core.module';
import { CuentasResolver } from './servicios/cuentas-resolver';

@NgModule({
  imports: [
    CoreModule,
    CuentasRoutingModule
  ],
  declarations: [
    CuentasInicioComponent
  ],
  providers: [
    CuentasServicio,
    CuentasResolver
  ]
})
export class CuentasModule { }
