import { defaultEnvironment } from './environment.default';

export const custom = {
  production: true,
  apiUrl: 'https://jorge-datos-service.herokuapp.com/'
};

export const environment = Object.assign(defaultEnvironment, custom);
